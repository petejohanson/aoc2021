#![feature(stdin_forwarders)]
use itertools::Itertools;
use std::io::{self};

fn main() {
    let larger = io::stdin()
        .lines()
        .map(|s| s.unwrap().parse::<i32>().unwrap())
        .tuple_windows::<(_, _)>()
        .filter(|(a, b)| a < b);

    println!("{:?}", larger.count());
}
