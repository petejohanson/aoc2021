#![feature(stdin_forwarders)]

use day3::{process_report_entry, report_entry_parser};

use std::io::{self};

fn main() {
    let mut entries = io::stdin()
        .lines()
        .map(|l| {
            let (_, e) = report_entry_parser(&l.unwrap()).unwrap();
            e
        })
        .peekable();

    let start = vec![0; entries.peek().unwrap().len()];

    let r = entries.fold(start, process_report_entry);

    let gamma = r.iter().rev().enumerate().fold(
        0,
        |agg, (bit, val)| if val > &0 { agg + (1 << bit) } else { agg },
    );
    let epsilon = r.iter().rev().enumerate().fold(
        0,
        |agg, (bit, val)| if val < &0 { agg + (1 << bit) } else { agg },
    );

    println!("{:?}", gamma * epsilon);
}
