#![feature(iter_zip)]

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::digit1,
    combinator::{map, map_res, value},
    multi::many1,
    IResult,
};

pub type ReportEntry = Vec<bool>;

pub type ReportTotal = Vec<i32>;

pub fn report_entry_parser(s: &str) -> IResult<&str, ReportEntry> {
    many1(alt((value(true, tag("1")), value(false, tag("0")))))(s)
}

use std::iter::zip;

pub fn process_report_entry(t: ReportTotal, e: ReportEntry) -> ReportTotal {
    t.iter()
        .zip(e.iter())
        .map(|(v, b)| match b {
            true => v + 1,
            false => v - 1,
        })
        .collect()
}
