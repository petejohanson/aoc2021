use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::digit1,
    combinator::{map, map_res, value},
    sequence::separated_pair,
    IResult,
};

#[derive(Clone, Debug)]
pub enum Direction {
    Forward,
    Down,
    Up,
}

#[derive(Debug)]
pub struct Move {
    pub dir: Direction,
    pub count: u32,
}

fn dir_parser(s: &str) -> IResult<&str, Direction> {
    alt((
        value(Direction::Forward, tag("forward")),
        value(Direction::Down, tag("down")),
        value(Direction::Up, tag("up")),
    ))(s)
}

fn dec_parser(s: &str) -> IResult<&str, u32> {
    map_res(digit1, |s: &str| s.parse::<u32>())(s)
}

pub fn move_parser(s: &str) -> IResult<&str, Move> {
    map(
        separated_pair(dir_parser, tag(" "), dec_parser),
        |(d, c)| Move { dir: d, count: c },
    )(s)
}
