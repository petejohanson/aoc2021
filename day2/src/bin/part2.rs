#![feature(stdin_forwarders)]

use day2::{move_parser, Direction, Move};

use std::io::{self};

#[derive(Debug, Default)]
struct Location {
    position: u32,
    depth: u32,
    aim: i32,
}

impl Location {
    fn apply_move(
        Location {
            position,
            depth,
            aim,
        }: Location,
        Move { dir, count }: Move,
    ) -> Self {
        match dir {
            Direction::Forward => Location {
                position: position + count,
                depth: ((depth as i32 + (aim * (count as i32))).try_into().unwrap()),
                aim,
            },
            Direction::Down => Location {
                position,
                depth,
                aim: (aim.wrapping_add(count as i32)),
            },
            Direction::Up => Location {
                position,
                depth: depth,
                aim: aim.wrapping_sub(count as i32),
            },
        }
    }
}

fn main() {
    let moves = io::stdin().lines().map(|l| {
        let (_rest, parsed) = move_parser(&l.unwrap()).unwrap();
        parsed
    });

    let Location {
        position, depth, ..
    } = moves.fold(Location::default(), Location::apply_move);

    println!("{}", position * depth);
}
