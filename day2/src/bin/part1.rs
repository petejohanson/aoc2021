#![feature(stdin_forwarders)]

use day2::{move_parser, Direction, Move};

use std::io::{self};

#[derive(Debug, Default)]
struct Location {
    position: u32,
    depth: u32,
}

impl Location {
    fn apply_move(Location { position, depth }: Location, Move { dir, count }: Move) -> Self {
        match dir {
            Direction::Forward => Location {
                position: position + count,
                depth: depth,
            },
            Direction::Down => Location {
                position: position,
                depth: depth + count,
            },
            Direction::Up => Location {
                position: position,
                depth: depth - count,
            },
        }
    }
}

fn main() {
    let moves = io::stdin().lines().map(|l| {
        let (_rest, parsed) = move_parser(&l.unwrap()).unwrap();
        parsed
    });

    let Location { position, depth } = moves.fold(Location::default(), Location::apply_move);

    println!("{}", position * depth);
}
